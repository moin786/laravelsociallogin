<?php
namespace SocialLogin\Login\Facades;

use Illuminate\Support\Facades\Facade;
class SiteLogin extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'sitelogin'; }
}