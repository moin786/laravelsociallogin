<?php

namespace peal\socialLogin\GithubLogin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Config\Repository;

class GithubLogin{

    /**
     * Github authorize url
     *
     * @var string
     */
    public $authorize_url;

    /**
     * Access token url
     *
     * @var string
     */
    public $token_url;

    /**
     * Github API url for developers
     *
     * @var string
     */
    public $api_base_url;

    /**
     * App client id
     *
     * @var string
     */
    public $client_id;

    /**
     * App client secret
     *
     * @var string
     */
    public $client_secret;

    /**
     * Redirect url
     *
     * @var string
     */
    public $redirect_uri;

    /**
     * Hold config array
     *
     * @var array
     */
    public $config;
    
    /**
     * Construct object
     */
    public function __construct(Repository $config){

        $this->config = $config;
        
    }

    /**
     * Get client id
     *
     * @return string
     */
    public function getClientId()
    {
        $this->client_id = $this->config->get('socialConfig.github.client_id');

        if(empty($this->client_id)){

            throw new \Exception('Required "client_id" key not supplied in config');
        }

        return $this;
    }


    /**
     * Get client secret
     *
     * @return string
     */
    public function getClientSecret()
    {
        $this->client_secret = $this->config->get('socialConfig.github.client_secret');

        if(empty($this->client_secret)){

            throw new \Exception('Required "client_secret" key not supplied in config');
        }

        return $this;
    }

    /**
     * Get redirect uri
     *
     * @return string
     */
    public function getRedirectUri()
    {
        $this->redirect_uri = $this->config->get('socialConfig.github.call_back_url');

        if (empty($this->redirect_uri)) {
            
            throw new \Exception('Required "call_back_url" key not supplied in config');
        }

        return $this;
    }


    /**
     * Get token url
     *
     * @return string
     */
    public function getTokenUrl() 
    {
        $this->token_url = $this->config->get('socialConfig.github.token_url');

        if (empty($this->token_url)) {
            
            throw new \Exception('Required "token_url" key not supplied in config');
        }

        return $this;
    }


    /**
     * Get authorize rul
     *
     * @return string
     */
    public function authorizeUrl()
    {
        $this->authorize_url = $this->config->get('socialConfig.github.authorize_url');

        if (empty($this->authorize_url)) {
            
            throw new \Exception('Required "authorize_url" key not supplied in config');
        }

        return $this;
    }
    
    /**
     * Get the authorize URL
     *
     * @returns a string
     */
    public function getAuthorizeURL($state){
        return $this->authorizeUrl() . '?' . http_build_query([
            'client_id' => $this->getClientId(),
            'redirect_uri' => $this->getRedirectUri(),
            'state' => $state,
            'scope' => 'user:email'
        ]);
    }
    

    /**
     * Exchange token and code for an access token
     *
     * @param mixed $state
     * @param mixed $oauth_code
     * @return string
     */ 
    public function getAccessToken(Request $request){

        if (!isset($request->state) || empty($request->get('state'))) {
            throw new \Exception('State can not left empty');
        }

        if (!isset($request->code) || empty($request->get('code'))) {
            throw new \Exception('Auth code can not left empty');
        }

        $token = static::apiRequest($this->getTokenUrl() . '?' . http_build_query([
            'client_id' => $this->getClientId(),
            'client_secret' => $this->getClientSecret(),
            'state' => $request->get('state'),
            'code' => $request->get('code')
        ]));

        Session::put('access_token', $token->access_token);

        return $token->access_token;
    }
    
   
    /**
     * Make an API request
     *
     * @param string $access_token_url
     * @return mixed
     */ 
    public static function apiRequest($access_token_url){

        $apiURL = filter_var($access_token_url, FILTER_VALIDATE_URL) ? $access_token_url : $this->api_base_url.'user?access_token='.$access_token_url;
        
        $context  = stream_context_create([
          'http' => [
            'user_agent' => 'GitHub OAuth Login',
            'header' => 'Accept: application/json'
          ]
        ]);
        
        $response = @file_get_contents($this->api_base_url, false, $context);
        
        return $response ? response()->json($response, 200) : $response;
    }

}