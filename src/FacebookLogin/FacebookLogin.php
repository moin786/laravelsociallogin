<?php

namespace peal\socialLogin\FacebookLogin;

use Facebook\Exceptions\FacebookAuthenticationException;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Support\Facades\Session;
use Facebook\Facebook;

class FacebookLogin {
    
    /**
     * Config repository
     *
     * @var Illuminate\Contracts\Config\Repository
     */
    protected $config;

    /**
     * Hold facebook instance
     *
     * @var Facebook\Facebook
     */
    protected $fb;

    /**
     * Hold callback url
     *
     * @var string
     */
    protected $callbackurl;

    /**
     * Hold 
     *
     * @var string
     */
    protected $helper;

    /**
     * Hold scopes
     *
     * @var array
     */
    protected $scope;

    /**
     * Booting services
     *
     * @param Facebook $fb
     * @param Repository $config
     */
    public function __construct(Facebook $fb, Repository $config) {

        $this->fb = $fb;
        $this->config = $config;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function redirectLoginUrl() {

        if ($this->fb instanceof Facebook) {

            return $this->fb->getRedirectLoginHelper();

        }

        throw new FacebookAuthenticationException("Facebook instance is missing");
    }
    public function setCallBackUrl() {

        $this->callbackurl = $this->config->get('socialConfig.facebook.call_back_url');

    }

    /**
     * Hold callback url
     *
     * @return string
     */
    protected function getCallBackUrl() {

        $this->callbackurl = $this->config->get('socialConfig.facebook.call_back_url');

        return $this->callbackurl;
    }

    /**
     * Return the redirect login helper
     *
     * @return FacebookRedirectLoginHelper
     */
    protected function getHelper() {

        return $this->redirectLoginUrl();

    }

    /**
     * Return the facebook login url
     *
     * @return string
     */
    public function getLoginUrl() {

        return $this->getHelper()
                    ->getLoginUrl(
                        $this->getCallBackUrl(), 
                        $this->getScope()
                    );
    }

    public function setScope() {
        $this->scope = $this->config->get('socialConfig.facebook.scope');
    }

    /**
     * Hold scopes
     *
     * @return array
     */
    protected function getScope() {

        $this->scope = $this->config->get('socialConfig.facebook.scope');

        return (array) $this->scope;
    }


    /**
     * Get access token
     *
     * @return FaceBookPersistentDataHandler
     */
    public function getAccessToken() {

        if (isset($_GET['state'])) {
            $this->getHelper()
                 ->getPersistentDataHandler()
                 ->set('state', $_GET['state']);
        }

        return $this->getHelper()
                    ->getAccessToken(
                        $this->config->get('socialConfig.facebook.call_back_url')
                    );
    }


    /**
     * Get OAuth2Client
     *
     * @return OAuth2Client
     */
    public function getOAuth2Client() {
        $token = $this->getAccessToken('');

        if (!$token->isLongLived()) {

            $oauth_client = $this->fb->getOAuth2Client();

            // Extend the access token.
            try {
                
                $token = $oauth_client->getLongLivedAccessToken($token);
               
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                
                return $e->getMessage();
            }

            $this->fb->setDefaultAccessToken($token);
        }

        Session::put('fb_user_access_token', (string) $token);

        try {

            $response = $this->fb->get('/me?fields=id,name,email,photos', Session::get('fb_user_access_token'));

        } catch (Facebook\Exceptions\FacebookSDKException $e) {

            return $e->getMessage();

        }

        // Convert the response to a `Facebook/GraphNodes/GraphUser` collection
        return $userinfo = $response->getGraphUser();

    }
}