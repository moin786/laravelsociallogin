<?php

namespace SocialLogin\Login\SiteLogin;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class SiteLogin{
    protected $model;
    
    public function __construct($model) {
        $this->model = $model;
    }
    
    public function getModel() {
        if (!isset($this->model)) {
            throw new Exception('Please set the model');
        }
        return $this->model;
        
    }
    
    public function create($data) {
        $model = $this->getModel();
        return $model::create($data);
    }
    
    public function memberInformation(){
        $ip = $_SERVER['REMOTE_ADDR']; 
        $query = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip));
        if ($query && $query['status'] == "success") {
            return $query;
        }
        throw new Exception("Something went wrong! network error, check your network");
    }
    
}
