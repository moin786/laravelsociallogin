<?php
return [
    'facebook' => [
        'app_id' => '488874124789189', // Replace {app-id} with your app id
        'app_secret' => '126dbd8b1f2b0efc9eadc552b44cbb94',
        'default_graph_version' => 'v2.2',
        'call_back_url' => 'https://grubdealz.com/members-login',
        'scope' => [
            'email'
        ]
    ],
    'google' => [
        'application_name' => 'restaurant-map-api',
        'client_id' => '213909393314-s8tdbi3shbk27c8t1ubff762482cl35o.apps.googleusercontent.com',
        'client_secret' => 'KFubXGdsmSAcDTX7FPs0xtY4',
        'redirect_url' => 'https://grubdealz.com/members-login',
        'scope' => [
            'https://www.googleapis.com/auth/plus.me',
            'https://www.googleapis.com/auth/userinfo.email',
            'https://www.googleapis.com/auth/userinfo.profile',
        ]
    ],

    'github' => [
        'app_id' => 44802,
        'client_id' => 'Iv1.ffee07237d301cae',
        'client_secret' => '5f84ecb9dbf8a4823821c6afac8c301cf271db30',
        'authorize_url' => 'https://github.com/login/oauth/authorize',
        'token_url' => 'https://github.com/login/oauth/access_token',
        'api_url_base' => 'https://api.github.com/',
        'call_back_url' => 'http://localhost/gitlogin/gitsuccess.php'
    ],
    'sitelogin' => 'App\MemberLogin'
];